
const tools = {
  // cookie 操作
  cookie: {
    /**
     * 读取cookie
     * @param {String} name cookie名
     * @returns {String} 返回对应kookie值或null
     */
    get: function(name) {
      let reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
      let arr = document.cookie.match(reg);
      let v = arr ? unescape(arr[2]) : null;
      return v;
    },

    /**
     * 写入cookie
     * @param {String} key cookie名
     * @param {String} value cookies值
     * @param {Number} date 到期时间(年/月/日 时:分:秒)
     * @param {String} path cookie路径
     */
    set: function(key, value, date, path) {
      let oDate = new Date(date);
      document.cookie = key + '=' + value + ';expires=' + oDate.toUTCString() + ';path=' + path;
    },

    /**
     * 删除cookie
     * @param {String} key cookie名
     */
    delete: function(key) {
      let oDate = new Date();
      oDate.setTime(oDate.getTime() - 1);
      document.cookie = key + '=;expires=' + oDate.toUTCString();
    }
  },

  /**
   * 生成指定大小随即数
   * @param {Number} Min 最小值
   * @param {Number} Max 最大值
   * @returns {Number} 随机数
   */
  createRandomNumber: function(Min,Max){
    let Range = Max - Min;
    let Rand = Math.random();
    let num = Min + Math.round(Rand * Range);
    return num;
  },

  /**
   * 生成指定长度字符串
   * @param {Number} len 
   * @param {String} type number letter mix
   */
  createRandomString: function(len = 1, type = 'mix') {
    const num = '0123456789';
    const letter = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    let str = '';
    let result = '';

    if (type == 'number') str = num;
    if (type == 'letter') str = letter;
    if (type == 'mix') str = num + letter;
    for (let i = len; i > 0; --i) result += str[Math.floor(Math.random() * str.length)];
    return result;
  },

  /**
   * 日期格式化
   * @param {Date} date 需格式化的日期
   * @param {String} fmt 所需的格式
   * @returns {String} 格式化后的日期字符串
   */
  dateFormat: function(date, fmt) {
    let o = {
      "M+" : date.getMonth()+1,
      "d+" : date.getDate(),
      "h+" : date.getHours(),
      "m+" : date.getMinutes(),
      "s+" : date.getSeconds(),
      "q+" : Math.floor((date.getMonth()+3)/3),
      "S+" : date.getMilliseconds()
    };
    if(/(y+)/.test(fmt)) {
      fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(let k in o) {
      if(new RegExp("("+ k +")").test(fmt)) {
        if (k == 'S+') {
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1 || RegExp.$1.length==2) ? (o[k]) : (("000"+ o[k]).substr((""+ o[k]).length)));
        } else {
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
      }
    }
    return fmt;
  },

  /**
   * url参数转json
   * @param {String} kvStr key=value形式的字符串
   * @returns {JSON} 转换后的json参数
   */
  urlParam2json: function(kvStr) {
    let paramArr = kvStr.split('&');
    let json = {};

    for (let i of paramArr) {
      let kvArr = i.split('=');
      json[kvArr[0]] = kvArr[1]
    }
    return json;
  },

  /**
   * json转url参数
   * @param {JSON} json json参数
   * @param {String} isSort 是否按母顺序进行排序
   * @returns {String} 转换后的url参数
   */
  json2urlParam: function(json, isSort = false) {
    let paramArr = [];

    for (let k in json) {
      paramArr.push(k + '=' + json[k]);
    }
    isSort ? paramArr.sort() : "";

    return paramArr.join('&');
  },

  /**
   * 获取设备id
   * @returns {String} 32位设备id
   */
  getLocaeDeviceId: function() {
    let deviceId = localStorage.getItem('deviceId');

    deviceId == null ? deviceId = this.createRandomString(32, 'mix') : '';
    localStorage.setItem('deviceId', deviceId);

    return localStorage.getItem('deviceId');
  },

  /**
   * 获取时区
   * @returns {String} 用户时区
   */
  getTimeZone: function() {
    return Intl.DateTimeFormat().resolvedOptions().timeZone;
  },

  /**
   * 获取时间戳
   * @returns {String} yyyyMMddhhmmss格式时间戳
   */
  getTimestamp: function() {
    return this.dateFormat(new Date(), 'yyyyMMddhhmmss');
  },

  /**
   * 生成MD5签名
   * @param {JSON} obj 要签名的数据
   * @param {String} signKey 签名key
   * @returns {String} 生成的签名
   */
  makeSign: function(obj, signKey) {
    let data = this.json2urlParam(obj, true);
    let sign = md5(data + '&key=' + signKey);

    return sign;
  },

  /**
   * 校验签名
   * @param {JSON} obj 要签名的数据
   * @param {JSON} signKey 签名key
   * @param {JSON} sign 校验的签名
   * @returns {Boolean} 校验结果
   */
  checkSign: function(obj, signKey, sign) {
    let dataSign = this.makeSign(obj, signKey);

    return dataSign == sign;
  },

  /**
   * 校验请求响应数据签名
   * @param {JSON} resData 要签名的数据和要校验的签名
   * @param {JSON} signKey 签名key
   * @returns {Boolean} 校验结果
   */
  checkResDataSign: function(resData, signKey) {
    let data = JSON.parse(JSON.stringify(resData));
    let sign = data.sign;
  
    sign != undefined ? delete data.sign : '';
  
    return tools.checkSign(data, signKey, sign);
  }

}