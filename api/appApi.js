import { API_userOnline, API_usmsMatchRegular, API_smsAutoConfirm,  API_pushMsg } from './index';
import { userInfo } from '../utils/storageTool';

// app心跳检测
const userOnline = function() {
  if (userInfo.get() != null) {
    API_userOnline().then(() => {
      // alert('userOnline 接口数据获取成功，data:\n' + res)
    }).catch(() => {
      // alert('userOnline 接口数据获取失败')
    })
  }
}

// 拉取拦截短信的正则表达式
const smsMatchRegular = function() {
  if (userInfo.get() != null) {
    API_usmsMatchRegular().then(data => {
      try {
        window.androidMethods.onSmsMatchRegular(data);
        // alert('已通过 androidMethods.onSmsMatchRegular 方法传数据给客户端，数据内容：' + data);
      } catch(err) {
        // alert('调用 androidMethods.onSmsMatchRegular 失败, 数据内容：' + data);
      }
    }).catch(() => {
      // alert('smsMatchRegular 接口数据获取失败')
    })
  }
}

// 上传短信自动确认
const smsAutoConfirm = function(data) {
  if (userInfo.get() != null) {
    // alert('传入参数:' + data);
    // 如果传的是json字符串，转为json格式
    typeof(data) == 'string' ?  data = JSON.parse(data) : '';
    API_smsAutoConfirm(data).then(() => {
      // alert('发送smsAutoConfirm数据成功，数据内容：' + JSON.stringify(data));
    }).catch((err) => {
      console.log(err);
      // alert('发送smsAutoConfirm数据成功，但服务端返回错误,错误代码:' + err.data.code + ' ' + err.data.desc);
    })
  }
}

// 推送
const pushNotify = function() {
  API_pushMsg().then(data => {
    try{
      window.androidMethods.onNotify(data);
      // alert('已通过 androidMethods.onNotify 方法传数据给客户端, 数据内容：' + data);
    } catch(err) {
      // alert('调用 androidMethods.onNotify 失败, 数据内容：' + data);
    }
  }).catch(() => {
    // alert('pushNotify 接口数据获取失败')
  })
}

// 设置推送
let timeID;
const setPushMsgCheck = function(time) {
  if (userInfo.get() != null) {
    // 获取登录时返回的推送时间
    let pushTime = userInfo.get('pushTime');
    // 如果参数中有时间信息，覆盖之
    time != undefined && time != '' ? pushTime = time : '';
    // 推送时间不为空时设置推送
    if (pushTime != null) {
      clearInterval(timeID);
      timeID = setInterval(() => {
        // 还在登录状态下，发送请求
        if (userInfo.get() != null) {
          pushNotify();
        } else {
          clearInterval(timeID);
        }
      }, pushTime)
    } else {
      clearInterval(timeID);
    }
  } else {
    clearInterval(timeID);
  }
}


const jsMethods = {
  userOnline,
  smsMatchRegular,
  smsAutoConfirm,
  pushNotify,
  setPushMsgCheck
}

export default jsMethods
