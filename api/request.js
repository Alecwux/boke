import reqConfig from './requestConfig';
import tools from '../utils/tools';
import { accessToken, saveSignInInfo } from '../utils/storageTool';
import axios from 'axios';
import router from '../router';

// 处理请求所需信息
const processRequestInfo = function(obj) {
  // 头信息
  let headerData = {
    appId: reqConfig.appId,
    apiVersion: reqConfig.apiVersion,
    deviceId: tools.getLocaeDeviceId(),
    timestamp: tools.getTimestamp(),
    timeZone: tools.getTimeZone()
  };
  // 请求数据
  let bodyData = obj.config.data == undefined ? {} : obj.config.data;
  // 需签名的数据
  let signData = {};
  Object.assign(signData, headerData, bodyData);
  // 生成签名
  let sign = tools.makeSign(signData, obj.signKey);
  
  // 添加头信息
  Object.assign(obj.config.headers, headerData);
  // 添加签名
  obj.config.headers.sign = sign;
  // 添加 Authorization
  obj.config.headers.Authorization = obj.Authorization;
  // get方法使用params传参
  if (obj.config.method == 'get') {
    obj.config.params = bodyData;
  }
  // from表单形式提交参数转为url形式
  if (obj.config.headers['Content-Type'] == 'application/x-www-form-urlencoded') {
    obj.config.data = tools.json2urlParam(bodyData);
  }
  return obj;
}


// 无需校验登录的 axios实例
const notSignInService = axios.create({
  baseURL: reqConfig.apiURL,
  method: 'post',
  timeout: 10000,
  headers: {'Content-Type': 'application/json'}
});
// 无需校验登录实例的请求拦截
notSignInService.interceptors.request.use(
  config => {
    // 处理请求所需信息
    processRequestInfo({
      config,
      Authorization: reqConfig.basicAuthorization,
      signKey: config.signKey
    });
    return config;
  },
  err  => {
    return Promise.reject(err);
  }
);
// 无需校验登录实例的响应拦截
notSignInService.interceptors.response.use(
  res => {
    // 业务码为1000,解析数据
    if (res.data.code == 10000) {
      // 判断是否需要验签
      if (res.data.sign != undefined && res.data.sign != '') {
        if (tools.checkResDataSign(res.data, res.config.signKey)) {
          return JSON.parse(decodeURIComponent(res.data.data));
        } else {
          return Promise.reject({type:'data', code:'10009', desc:'Data sign error'});
        }
      } else {
        return res.data.data;
      }
    } else {
      return Promise.reject({type:'service', code:res.data.code, desc:res.data.desc});
    }
  },
  err => {
    if (err.response) {
      return Promise.reject({type:'request', code:err.response.status, desc:err.response.statusText == '' ? 'Request Error' : err.response.statusText});
    } else {
      return Promise.reject({type:'network', code:null, desc:err.message});
    }
  }
);
// 无需校验登录请求方法
const unSignInRequest = function(o) {
  let config = {
    signKey: localStorage.getItem('deviceKey')
  };
  Object.assign(config, o);
  return new Promise((resolve, reject) => {
    // 通过实例发送请求
    notSignInService(config).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};


// 获取设备key方法
const getDeviceKey = function() {
  return new Promise((resolve, reject) => {
    unSignInRequest({
      url: '/reqDeviceKey',
      signKey: reqConfig.appSign
    }).then(res => {
      // 保存设备key到 localStorage
      localStorage.setItem('deviceKey', res.deviceKey);
      // 检查是否保存到 localStorage
      localStorage.getItem('deviceKey') == null ? res.saveLocal = false : res.saveLocal = true;
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};

// 刷新token方法
const refreshToken = function() {
  return new Promise((resolve, reject) => {
    unSignInRequest({
      url: '/refreshToken',
      data: {
        refreshToken: localStorage.getItem('refresh_token'),
        grantType: 'refresh_token'
      }
    }).then(res => {
      saveSignInInfo(res);
      resolve(res);
    }).catch(err => {
      // 获取token失败跳转登录页
      router.push('/signIn');
      reject(err);
    })
  });
};


// 需校验登录的 axios实例
const signInService = axios.create({
  baseURL: reqConfig.apiURL,
  method: 'post',
  timeout: 10000,
  headers: {'Content-Type': 'application/json'}
});
// 需校验登录实例的请求拦截
signInService.interceptors.request.use(
  config => {
    // 处理请求所需信息
    processRequestInfo({
      config,
      Authorization: accessToken.get(),
      signKey: localStorage.getItem('deviceKey')
    });
    return config;
  },
  error  => {
    return Promise.reject(error);
  }
);
// 需校验登录实例的响应拦截
signInService.interceptors.response.use(
  res => {
    // 业务码为1000,解析数据
    if (res.data.code == 10000) {
      // 判断是否需要验签
      if (res.data.sign != undefined && res.data.sign != '') {
        if (tools.checkResDataSign(res.data, localStorage.getItem('deviceKey'))) {
          // 判断是否需要返回原数据
          if (res.config.originalData) {
            return res.data.data;
          } else {
            return JSON.parse(decodeURIComponent(res.data.data));
          }
        } else {
          return Promise.reject({type:'data', code:'10009', desc:'Data sign error'});
        }
      } else {
        return res.data.data;
      }
    }  else {
      // 业务状态码为 10014 跳转登录页
      if (res.data.code == 10014) {
        router.push('/signIn');
      }
      return Promise.reject({type:'service', code:res.data.code, desc:res.data.desc});
    }
  },
  err => {
    if (err.response) {
      // 401 状态跳转登录页
      if (err.response.status == 401) {
        router.push('/signIn');
      }
      return Promise.reject({type:'request', code:err.response.status, desc:err.response.statusText == '' ? 'Request Error' : err.response.statusText});
    } else {
      return Promise.reject({type:'network', code:null, desc:err.message});
    }
  }
);
// 需校验登录请求方法
const needSignInRequest = function(o) {
  let config = {};
  Object.assign(config, o);
  return new Promise((resolve, reject) => {
    // 通过实例发送请求
    signInService(config).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};

export {
  reqConfig,
  getDeviceKey,
  refreshToken,
  unSignInRequest,
  needSignInRequest
}