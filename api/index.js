import {
	reqConfig,
	getDeviceKey,
	refreshToken,
	unSignInRequest,
	needSignInRequest
} from './request';
const api = {
	API_getDeviceKey() {
		return getDeviceKey()
	},
	API_refreshToken() {
		return refreshToken()
	},
	API_getCountryList() {
		return unSignInRequest({
			url: '/countryList'
		})
	},
	API_getSMS(data) {
		return unSignInRequest({
			url: '/code/sms',
			method: 'get',
			data
		})
	},
	API_getEmailSMS(data) {
		return unSignInRequest({
			url: '/code/email',
			method: 'get',
			data
		})
	},
	API_register(data) {
		return unSignInRequest({
			url: '/register',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data
		})
	},
	API_signInByPWD(data) {
		return unSignInRequest({
			url: '/authentication/form',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data
		})
	},
	API_signInBySMS(data) {
		return unSignInRequest({
			url: '/authentication/email',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data
		})
	},
	API_passwordForget(data) {
		return unSignInRequest({
			url: '/passwordForget',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data
		})
	},
	API_passwordUpdate(data) {
		return needSignInRequest({
			url: '/passwordUpdate',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data
		})
	},
	API_querySwitchStatus(data) {
		return needSignInRequest({
			url: '/querySwitchStatus',
			data
		})
	},
	API_getExchangerate(data) {
		return needSignInRequest({
			url: '/reqExchangerate',
			data
		})
	},
	API_getRechargeData(data) {
		return needSignInRequest({
			url: '/reqRechargeData',
			data
		})
	},
	API_upRechargeImage(data) {
		return needSignInRequest({
			url: '/trade/uploadRechargeImg',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			timeout: 1000 * 60 * 10,
			data
		})
	},
	API_submitRechargeInfo(data) {
		return needSignInRequest({
			url: '/trade/saveRechargeInfo',
			data
		})
	},
	API_getRechargeDataList(data) {
		return needSignInRequest({
			url: '/reqRechargeDataList',
			data
		})
	},
	API_switchStatus(data) {
		return needSignInRequest({
			url: '/switchStatus',
			data
		})
	},
	API_getOrderList(data) {
		return needSignInRequest({
			url: '/orderList',
			data
		})
	},
	API_getOrderDetail(data) {
		return needSignInRequest({
			url: '/orderDetail',
			data
		})
	},
	API_upOrderImage(data) {
		return needSignInRequest({
			baseURL: reqConfig.imageUpURL,
			url: '/orderImage',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			timeout: 1000 * 60 * 10,
			data
		})
	},
	API_orderArgue(data) {
		return needSignInRequest({
			url: '/orderArgue',
			data
		})
	},
	API_smsManualConfirm(data) {
		return needSignInRequest({
			url: '/smsManualConfirm',
			data
		})
	},
	API_orderConfirm(data) {
		return needSignInRequest({
			url: '/orderConfirm',
			data
		})
	},
	API_orderGrab(data) {
		return needSignInRequest({
			url: '/robRecycleOrder',
			data
		})
	},
	API_cardList(data) {
		return needSignInRequest({
			url: '/cardList',
			data
		})
	},
	API_upiAdd4IN(data) {
		return needSignInRequest({
			url: '/cardAdd/IN',
			data
		})
	},
	API_cardAdd4IN(data) {
		return needSignInRequest({
			url: '/cardAdd/IN',
			data
		})
	},
	API_cardAdd4ID(data) {
		return needSignInRequest({
			url: '/cardAdd',
			data
		})
	},
	API_cardDel(data) {
		return needSignInRequest({
			url: '/cardDel',
			data
		})
	},
	API_cardStatus(data) {
		return needSignInRequest({
			url: '/cardStatus',
			data
		})
	},
	API_cardCheck(data) {
		return needSignInRequest({
			url: '/cardCheck',
			data
		})
	},
	API_withdrawalList(data) {
		return needSignInRequest({
			url: '/withdrawalList',
			data
		})
	},
	API_withdrawalReq(data) {
		return needSignInRequest({
			url: '/withdrawalReq',
			data
		})
	},
	API_rechargedData(data) {
		return needSignInRequest({
			url: '/rechargedData',
			data
		})
	},
	API_rechargedList(data) {
		return needSignInRequest({
			url: '/rechargedList',
			data
		})
	},
	API_profitsDetails(data) {
		return needSignInRequest({
			url: '/profitsDetails',
			data
		})
	},
	API_profitsDetailsDownload(data) {
		return needSignInRequest({
			url: '/profitsDetails/download',
			originalData: true,
			data
		})
	},
	API_userAccount(data) {
		return needSignInRequest({
			url: '/userAccount',
			data
		})
	},
	API_userAccountDetail(data) {
		return needSignInRequest({
			url: '/userAccountDetail',
			data
		})
	},
	API_getBankList(data) {
		return needSignInRequest({
			url: '/bankList',
			data
		})
	},
	API_userOnline() {
		return needSignInRequest({
			url: '/userOnline',
			originalData: true
		})
	},
	API_usmsMatchRegular() {
		return needSignInRequest({
			url: '/smsMatchRegular',
			originalData: true
		})
	},
	API_smsAutoConfirm(data) {
		return needSignInRequest({
			url: '/smsAutoConfirm',
			data,
			originalData: true
		})
	},
	API_pushMsg() {
		return needSignInRequest({
			url: '/pushMsg',
			originalData: true
		})
	},
	API_getCustomerList(data) {
		return needSignInRequest({
			url: '/findSubUser',
			data
		})
	},
	API_updateProfitRate(data) {
		return needSignInRequest({
			url: '/updateProfitRate',
			data
		})
	}
}
export default api;
export const {
	API_getDeviceKey,
	API_refreshToken,
	API_getCountryList,
	API_getSMS,
	API_getEmailSMS,
	API_register,
	API_signInByPWD,
	API_signInBySMS,
	API_passwordForget,
	API_passwordUpdate,
	API_querySwitchStatus,
	API_getExchangerate,
	API_getRechargeData,
	API_upRechargeImage,
	API_submitRechargeInfo,
	API_getRechargeDataList,
	API_switchStatus,
	API_getOrderList,
	API_getOrderDetail,
	API_upOrderImage,
	API_orderArgue,
	API_smsManualConfirm,
	API_orderConfirm,
	API_orderGrab,
	API_cardList,
	API_upiAdd4IN,
	API_cardAdd4IN,
	API_cardAdd4ID,
	API_cardDel,
	API_cardStatus,
	API_cardCheck,
	API_withdrawalList,
	API_withdrawalReq,
	API_rechargedData,
	API_rechargedList,
	API_profitsDetails,
	API_profitsDetailsDownload,
	API_userAccount,
	API_userAccountDetail,
	API_getBankList,
	API_userOnline,
	API_usmsMatchRegular,
	API_smsAutoConfirm,
	API_pushMsg,
	API_getCustomerList,
	API_updateProfitRate
} = api;