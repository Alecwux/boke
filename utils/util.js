/**
 * @DateDiff 计算相差的天数
 * @param Date_end 结束时间
 * @param Date_start 开始时间
 * @returns {number} 相差天数
 */
export function DateDiff(Date_end, Date_start, abs){
  abs == undefined ? abs = true : false;
  let aDate, oDate1, oDate2, iDays;
  Date_end = Date_end.split(" "); //将时间以空格划分为两个数组  第一个数组是 2019-05-20 第二个数组是 00：00：00
  aDate = Date_end[0].split("-"); //获取第一个数组的值
  oDate1 = new Date(aDate[0], aDate[1], aDate[2]);  //将前半个数组以-拆分，每一个是一个数值
  Date_start = Date_start.split(" ");
  aDate = Date_start[0].split("-");
  oDate2 = new Date(aDate[0], aDate[1], aDate[2]);
  if (abs) {
    iDays = parseInt(Math.abs(oDate1 - oDate2) / 1000 / 60 / 60 / 24);    //把相差的毫秒数转换为天数
  } else {
    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 / 24);    //把相差的毫秒数转换为天数
  }
  return iDays;
}

/**
 * 日期格式化
 * @param {Date} date 需格式化的日期
 * @param {String} fmt 所需的格式
 * @returns {String} 格式化后的日期字符串
 */
export function dateFormat(date, fmt) {
  let o = {
    "M+" : date.getMonth()+1,
    "D+" : date.getDate(),
    "h+" : date.getHours(),
    "m+" : date.getMinutes(),
    "s+" : date.getSeconds(),
    "q+" : Math.floor((date.getMonth()+3)/3),
    "S+" : date.getMilliseconds()
  };
  if(/(Y+)/.test(fmt)) {
    fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
  }
  for(let k in o) {
    if(new RegExp("("+ k +")").test(fmt)) {
      if (k == 'S+') {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1 || RegExp.$1.length==2) ? (o[k]) : (("000"+ o[k]).substr((""+ o[k]).length)));
      } else {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
      }
    }
  }
  return fmt;
}

export function allSettled(promises) {
  let wrappedPromises = promises.map(p => Promise.resolve(p)
    .then (
      val => ({ status: 'fulfilled', value: val }),
      err => ({ status: 'rejected', reason: err })
    )
  );
  return Promise.all(wrappedPromises);
}