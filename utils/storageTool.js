/**
 * 本地存储json格式的获取、设置、添加、删除
 */
const jsonStorage = {
  /**
   * 读取
   * @param {String} key 本地存储的key
   * @param {String} item 本地存储json格式值中子项的属性名,为空获取全部值
   * @returns {JSON | *} 返回要获取的数据
   */
  get(key, item) {
    let data = JSON.parse(localStorage.getItem(key));

    if (data != null && item != undefined) {
      return data[item];
    } else {
      return data;
    }
  },
  /**
   * 设置
   * @param {String} key 本地存储的key
   * @param {JSON} data 要存储的json格式数据
   */
  set(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  },
  /**
   * 添加
   * @param {String} key 本地存储的key
   * @param {String} item 要添加数据的子项名
   * @param {*}} value 要添加数据的子项数据
   */
  add(key, item, value) {
    let localData = this.get(key);

    localData == null ? localData = {} : '';
    localData[item] = value;
    this.set(key, localData);
  },
  /**
   * 删除
   * @param {String} key 本地存储的key
   * @param {String} item 要删除子项的名,为空删除全部值
   */
  del(key, item) {
    if (item != undefined) {
      let localData = this.get(key);

      localData == null ? localData = {} : '';
      delete localData[item];      
      this.set(key, localData);
    } else {
      localStorage.removeItem(key);
    }
  }
}

/**
 * 本地存储json格式的获取、设置、添加、删除 工厂方法
 * @param {String} key 要实例化本地存储的key
 */
function JsonStorage(key) {
  this.key = key;
  this.get = (item) => {
    return jsonStorage.get(this.key, item);
  }
  this.set = (data) => {
    jsonStorage.set(this.key, data);
  }
  this.add = (item, value) => {
    jsonStorage.add(this.key, item, value);
  }
  this.del = (item) => {
    jsonStorage.del(this.key, item);
  }
}

/**
 * 有时效的本地存储写入读取
 */
const expireStorage = {
  /**
   * 有时效的本地存储写入
   * @param {String} key 本地存储key
   * @param {String | JSON | Array | Number} value 要存储的数据
   * @param {Number} expire 有效期
   */
  set(key, value, expire) {
    const obj = {
      data: value,
      expire: new Date().getTime() + expire
    };
    localStorage.setItem(key, JSON.stringify(obj));
  },
  /**
   * 有时效的本地存储读取
   * @param {String} key 本地存储key
   * @returns {*} 存储的数据
   */
  get(key) {
    const item = localStorage.getItem(key);
    const time = new Date().getTime();
    let result = null;

    if (item) {
      const obj = JSON.parse(item);
      if (time < obj.expire) {
        result = obj.data;
      } else {
        localStorage.removeItem(key);
      }
    }
    return result;
  }
}

// /**
//  * accessToken 存储、读取、删除
//  * 有过期时间
//  */
// const accessToken = {
//   /**
//    * 存储accessToken
//    * @param {String} value 要保存的accessToken
//    * @param {Number} expire 过期时间
//    */
//   set(value, expire) {
//     expireStorage.set('access_token', value, expire * 1000);
//   },
//   /**
//    * 读取accessToken
//    */
//   get() {
//     return expireStorage.get('access_token');
//   },
//   /**
//    * 删除accessToken
//    */
//   del() {
//     localStorage.removeItem('access_token');
//   }
// }

/**
 * accessToken 存储、读取、删除
 */
const accessToken = {
  /**
   * 存储accessToken
   * @param {String} data 要保存的accessToken
   */
  set(data) {
    localStorage.setItem('access_token', data);
  },
  /**
   * 读取accessToken
   */
  get() {
    return localStorage.getItem('access_token');
  },
  /**
   * 删除accessToken
   */
  del() {
    localStorage.removeItem('access_token');
  }
}

/**
 * refreshToken 存储、读取、删除
 */
const refreshToken = {
  /**
   * 存储refreshToken
   * @param {String} data 要保存的refreshToken
   */
  set(data) {
    localStorage.setItem('refresh_token', data);
  },
  /**
   * 读取refreshToken
   */
  get() {
    return localStorage.getItem('refresh_token');
  },
  /**
   * 删除refreshToken
   */
  del() {
    localStorage.removeItem('refresh_token');
  }
}

/**
 * 用户信息存储
 */
const userInfo = new JsonStorage('authUser');

/**
 * 保存登录信息
 * @param {JSON} data 服务端返回的登录信息
 */
const saveSignInInfo = (data) => {
  // 存储accessToken
  accessToken.set(data.token_type + ' ' + data.access_token);
  // 存储refreshToken
  refreshToken.set(data.refresh_token);
  // 存储用户信息
  let userData = {}
  Object.assign(userData, data);
  delete userData.access_token;
  delete userData.refresh_token;
  delete userData.expires_in;
  delete userData.token_type;
  userInfo.set(userData);
}

/**
 * 删除登录信息
 */
const removeSignInInfo = () => {
  accessToken.del();
  refreshToken.del();
  userInfo.del();
}

 
export {
  accessToken,
  refreshToken,
  expireStorage,
  JsonStorage,
  userInfo,
  saveSignInInfo,
  removeSignInInfo
}