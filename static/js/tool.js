/**
 * 倒计时
 * @param {*} option 
 */
function MSCountDown(option) {
  var _this = this;
  var config = {
    second: 60,  // 默认倒计时秒数
    autoReady: true,  // 是否自动准备好, 默认true
    onReady: function(){},  // 准备好事件
    onStart: function(){},  // 开始倒计时事件
    onProgress: function(){},  // 正在倒计时事件
    onEnd: function(){},  // 倒计时结束事件
  };
  var count = null;  // 内部倒计时专用值
  this.option = option;  // 配置项
  this.id = null;   // setInterval 的 id
  this.second = null;  // 当前秒数
  this.status = null;  // 倒计时状态, init | ready | start | stop | pause | progress

  // 倒计时
  function countDown() {
    _this.status = 'progress';
    config.onProgress.call(_this, _this.timeCompute(count));
    if (count > 0) {
      _this.id = setInterval(function() {
        _this.second = --count;
        if (count >= 0) {
          config.onProgress.call(_this, _this.timeCompute(count));
          if (count == 0) {
            clearInterval(_this.id);
            _this.stop();
            config.onEnd.call(_this, _this.timeCompute(count));
          }
        } else {
          clearInterval(_this.id);
        }
      }, 1000);
    } else {
      _this.stop();
      config.onEnd.call(_this, _this.timeCompute(count));
    }
  };
  
  // 设置数据
  function setData() {
    var opt = _this.option;
    typeof opt.autoReady === 'boolean' ? config.autoReady = opt.autoReady : null;
  };
  // 设置事件
  function setEvent() {
    var opt = _this.option;
    typeof opt.onReady === 'function' ? config.onReady = opt.onReady : null;
    typeof opt.onStart === 'function' ? config.onStart = opt.onStart : null;
    typeof opt.onProgress === 'function' ? config.onProgress = opt.onProgress : null;
    typeof opt.onEnd === 'function' ? config.onEnd = opt.onEnd : null;
  };
  // 准备好
  function ready() {
    var opt = _this.option;
    _this.second = count = isNaN(parseInt(opt.second)) ? config.second : parseInt(opt.second);
    _this.status = 'ready';
    config.onReady.call(_this, _this.timeCompute(count));
  };
  // 初始化
  function init() {
    _this.status = 'init';
    typeof _this.option.onInit === 'function' ? _this.option.onInit.call(_this) : null;
    setData();
    setEvent();
    if (config.autoReady) {
      ready();
    } else {
      _this.ready = ready;
    }
  };
  // 秒数转天时分秒
  this.timeCompute = function(second) {
    var d,h,m,s;
    d = Math.floor(second / 60 / 60 / 24);
    h = Math.floor(second / 60 / 60 % 24);
    m = Math.floor(second / 60 % 60);
    s = Math.floor(second % 60);
    return {day:d, hour:h, minute:m, second:s,};
  },
  // 开始方法
  this.start = function(second) {
    if (this.status == 'ready') {
      var temp = isNaN(parseInt(second)) ? count : parseInt(second);
      if (temp > 0) {
        isNaN(parseInt(second)) ? null : this.second = count = parseInt(second);
        this.status = 'start';
        config.onStart.call(this, this.timeCompute(count));
        countDown();
      } else {
        console.error('Seconds must be greater than 0.');
      }
    } else {
      return false;
    }
  };
  // 停止方法
  this.stop = function() {
    if (this.status != 'init' && this.status != 'ready' && this.status != 'stop') {
      clearInterval(this.id);
      this.status = 'stop';
      if (config.autoReady) {
        setTimeout(function() {
          ready();
        }, 800);
      }
    } else {
      return false;
    }
  };
  // 暂停方法
  this.pause = function() {
    if (this.status == 'progress') {
      clearInterval(this.id);
      this.status = 'pause';
    } else {
      return false;
    }
  };
  // 继续方法
  this.continue = function() {
    if (this.status == 'pause') {
      countDown();
    } else {
      return false;
    }
  };
  // 重置方法
  this.reset = function() {
    clearInterval(this.id);
    init();
  };
  init();
};

/**
 * 日期格式化
 * @param {Date} date 需格式化的日期
 * @param {String} reg 目标格式，可选，默认：yyyy-MM-ddTHH:mm:ss
 * @returns {String} 格式化后的日期字符串
 */
function dateFormat(date, reg) {
  typeof(date) == 'string' && date.includes('-') ? date = date.replace(' ', 'T') : null;  // 传入的日期是字符串且日期以“-”分割时，将日期与时间的空格分割换为T分割
  var _date = new Date(date);
  if (_date != 'Invalid Date') {  // 传入的日期是否能转化为日期格式
    typeof(reg) != 'string' ? reg = 'yyyy-MM-ddTHH:mm:ss' : reg;  // 设置默认的日期格式
    var offset = _date.getTimezoneOffset();  // 获取本地时间与用UTC表示当前日期的时间差，以分钟为单位
    var map = {};
    function zeroFill(number, targetLength) {  // 数字补零
      var absNumber = '' + Math.abs(number);
      var zerosToFill = targetLength - absNumber.length;
      return (number >= 0 ?  '' : '-') + Math.pow(10, Math.max(0, zerosToFill)).toString().substring(1) + absNumber;
    };
    map.yyyy = _date.getFullYear();  // 年（四位）
    map.yy = ('' + map.yyyy).substring(2); // 年（后两位）
    map.M = _date.getMonth() + 1; // 月
    map.MM = zeroFill(map.M, 2);  // 月（补零）
    map.d = _date.getDate();  // 日
    map.dd = zeroFill(map.d, 2);  // 日（补零）
    map.H = _date.getHours();  // 时 - 24制
    map.HH = zeroFill(map.H, 2);  // 时 - 24制（补零）
    map.h = map.H % 12 === 0 ? 12 : map.H % 12;  // 时 - 12制
    map.hh = zeroFill(map.h, 2);  // 时 - 12制（补零）
    map.m = _date.getMinutes();  // 分
    map.mm = zeroFill(map.m, 2);  // 分（补零）
    map.s = _date.getSeconds();  // 秒
    map.ss = zeroFill(map.s, 2);  // 秒（补零）
    map.S = _date.getMilliseconds();  // 毫秒
    map.SSS = zeroFill(map.S, 3);  // 毫秒（补零）
    map.a = map.H < 12 ? 'am' : 'pm';  // 12制上午、下午（小写）
    map.A = map.H < 12 ? 'AM' : 'PM';  // 12制上午、下午（大写）
    map.z = 0 - offset / 60;  // 时区（以时为单位的小数）
    map.Z = (map.z < 0 ? '-' : '+') + zeroFill(parseInt(Math.abs(offset) / 60), 2) + ':' + zeroFill(Math.abs(offset) % 60, 2);  // 时区（时间显示 :分隔）
    map.ZZ = map.Z.replace(':', '');  // 时区（时间显示 无分隔符）
    map.x = _date.getTime();  // 时间戳（精确到毫秒）
    map.X = Date.parse(_date);  // 时间戳（精确到秒）
    map.E = _date.getDay() || 7;  // 星期
    map.Q = Math.floor((map.M + 2) / 3);  // 季度
    return reg.replace(/\byyyy|yy|MM|M|dd|d|HH|H|hh|h|mm|m|ss|s|SSS|S|a|A|z|ZZ|Z|x|X|E|Q\b/g, $1 => map[$1]);
  } else {
    return _date;
  }
}
/**
 * 数字补零
 * @param {Number} number 要补零的数字
 * @param {Number} targetLength 目标位数 
 * @returns {String} 补零后数字字符串
 */
function zeroFill(number, targetLength) {  // 数字补零
  var absNumber = '' + Math.abs(number);
  var zerosToFill = targetLength - absNumber.length;
  return (number >= 0 ?  '' : '-') + Math.pow(10, Math.max(0, zerosToFill)).toString().substring(1) + absNumber;
};
